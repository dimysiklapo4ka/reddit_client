package dimysiklapo4ka.com.domain.models

class ListingResponse(val data: ListingData?,
                      val message: String?,
                      val error: Int?)

class ListingData(
    val children: List<RedditChildrenResponse>,
    val after: String?,
    val before: String?
)

data class RedditChildrenResponse(val data: RedditTopModel)

data class RedditTopModel(
    val name: String?,
    val title: String?,
    val selftext:String?,
    val score: Int?,
    val author: String?,
    val subreddit: String?,
    val num_comments: Int?,
    val created_utc: Long?,
    val thumbnail: String?,
    val url: String?)