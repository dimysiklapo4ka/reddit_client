package dimysiklapo4ka.com.domain

import dimysiklapo4ka.com.domain.interactors.RedditInteractor
import dimysiklapo4ka.com.domain.interactors.RedditInteractorImpl
import org.koin.dsl.module.module
import org.koin.experimental.builder.singleBy

val domainInjector = module {
    singleBy<RedditInteractor, RedditInteractorImpl>()
}