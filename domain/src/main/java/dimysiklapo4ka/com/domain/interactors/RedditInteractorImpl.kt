package dimysiklapo4ka.com.domain.interactors

import dimysiklapo4ka.com.domain.geteways.RedditGeteway
import dimysiklapo4ka.com.domain.models.ListingResponse
import dimysiklapo4ka.com.domain.models.RedditTopModel

class RedditInteractorImpl(private val repository: RedditGeteway): RedditInteractor {

    override suspend fun insert(redditData: List<RedditTopModel>) =
        repository.insert(redditData)

    override suspend fun delete(redditData: List<RedditTopModel>) =
        repository.delete(redditData)

    override suspend fun getAllRedditData(): List<RedditTopModel>? =
        repository.getAllRedditData()

    override suspend fun getTopRedditPosts(subreddit: String, pageSize: Int): ListingResponse? =
        repository.getTopRedditPosts(subreddit, pageSize)

    override suspend fun getTopRedditPostsAfter(subreddit: String, after: String, pageSize: Int): ListingResponse? =
        repository.getTopRedditPostsAfter(subreddit, after, pageSize)

    override suspend fun getTopRedditPostsBefore(subreddit: String, before:String, pageSize: Int): ListingResponse? =
            repository.getTopRedditPostsBefore(subreddit, before, pageSize)

    override suspend fun clearTable() = repository.clearTable()
}