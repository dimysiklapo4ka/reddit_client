package dimysiklapo4ka.com.domain.interactors

import dimysiklapo4ka.com.domain.models.ListingResponse
import dimysiklapo4ka.com.domain.models.RedditTopModel

interface RedditInteractor {
    suspend fun getTopRedditPosts(subreddit: String, pageSize: Int): ListingResponse?
    suspend fun getTopRedditPostsAfter(subreddit: String, after: String, pageSize: Int): ListingResponse?
    suspend fun getTopRedditPostsBefore(subreddit: String, before: String, pageSize: Int): ListingResponse?
    suspend fun insert(redditData: List<RedditTopModel>)
    suspend fun delete(redditData: List<RedditTopModel>)
    suspend fun getAllRedditData(): List<RedditTopModel>?
    suspend fun clearTable()
}