package dimysiklapo4ka.com.data

import androidx.room.Room
import dimysiklapo4ka.com.data.datasource.api.RetrofitClient
import dimysiklapo4ka.com.data.datasource.database.DB_TABLE_NAME
import dimysiklapo4ka.com.data.datasource.database.RedditDb
import dimysiklapo4ka.com.data.repository.RedditRepository
import dimysiklapo4ka.com.domain.geteways.RedditGeteway
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module
import org.koin.experimental.builder.singleBy

val dataInjector = module {
    single { Room.databaseBuilder(androidApplication(), RedditDb::class.java, DB_TABLE_NAME).build() }

    single { get<RedditDb>().getRedditDb() }

    singleBy<RedditGeteway, RedditRepository>()

    single { RetrofitClient().getRedditApi() }

}