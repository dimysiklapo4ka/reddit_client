package dimysiklapo4ka.com.data.datasource.database

import androidx.room.Database
import androidx.room.RoomDatabase
import dimysiklapo4ka.com.data.datasource.database.model.RedditEntity

@Database(
    entities = [RedditEntity::class],
    version = DB_VERSION,
    exportSchema = false
)
abstract class RedditDb: RoomDatabase() {
    abstract fun getRedditDb(): RedditDAO
}