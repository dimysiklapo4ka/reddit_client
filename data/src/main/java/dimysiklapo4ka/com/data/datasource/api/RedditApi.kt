package dimysiklapo4ka.com.data.datasource.api

import dimysiklapo4ka.com.data.datasource.api.model.DataListingResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RedditApi {
    @GET(REDDIT_TOP)
    fun getTop(
        @Path("subreddit") subreddit: String,
        @Query("limit")limit: Int
    ): Deferred<Response<DataListingResponse>>

    @GET(REDDIT_TOP)
    fun getTopAfter(
        @Path("subreddit") subreddit: String,
        @Query("after") after: String,
        @Query("limit")limit: Int
    ): Deferred<DataListingResponse>

    @GET(REDDIT_TOP)
    fun getTopBefore(
        @Path("subreddit") subreddit: String,
        @Query("before") before: String,
        @Query("limit")limit: Int
    ): Deferred<DataListingResponse>
}