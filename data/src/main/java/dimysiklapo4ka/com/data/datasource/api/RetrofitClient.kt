package dimysiklapo4ka.com.data.datasource.api

import android.util.Log
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient{

    fun getRedditApi(): RedditApi{
        return retrofitClient.create(RedditApi::class.java)
    }

    companion object{
        private const val BASE_URL = "https://www.reddit.com/"

        private val logger = HttpLoggingInterceptor(/*HttpLoggingInterceptor.Logger {
            Log.e("REDDIT_API", it)
        }*/)
        private val client = OkHttpClient.Builder()
            .addInterceptor(logger.setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
        private val retrofitClient: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .build()
    }
}