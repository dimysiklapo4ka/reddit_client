package dimysiklapo4ka.com.data.datasource.api.model

import dimysiklapo4ka.com.data.datasource.database.model.RedditEntity

class DataListingResponse(val data: ListingData?,
                          val message: String?,
                          val error: Int?)

class ListingData(
    val children: List<RedditChildrenResponse>,
    val after: String?,
    val before: String?
)

data class RedditChildrenResponse(val data: RedditEntity)