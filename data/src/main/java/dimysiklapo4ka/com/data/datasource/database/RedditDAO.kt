package dimysiklapo4ka.com.data.datasource.database

import androidx.room.*
import dimysiklapo4ka.com.data.datasource.database.model.RedditEntity

@Dao
interface RedditDAO {
    //Added RedditEntity in Database
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(redditData: List<RedditEntity>)

    //Delete RedditEntity of Database
    @Delete
    fun delete(redditData: List<RedditEntity>)

    //Get all RedditEntity of Database
    @Query("SELECT * FROM reddit_db")
    fun getAllRedditData(): List<RedditEntity>

    @Query("DELETE FROM reddit_db")
    fun nukeTable()

}