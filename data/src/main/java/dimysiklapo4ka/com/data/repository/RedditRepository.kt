package dimysiklapo4ka.com.data.repository

import android.util.Log
import com.google.gson.Gson
import dimysiklapo4ka.com.data.datasource.api.RedditApi
import dimysiklapo4ka.com.data.datasource.api.model.DataListingResponse
import dimysiklapo4ka.com.data.datasource.database.RedditDb
import dimysiklapo4ka.com.data.repository.mappers.RedditMapper
import dimysiklapo4ka.com.domain.geteways.RedditGeteway
import dimysiklapo4ka.com.domain.models.ListingResponse
import dimysiklapo4ka.com.domain.models.RedditTopModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response

class RedditRepository(
    database: RedditDb,
    private val api: RedditApi
): RedditGeteway {

    private val databaseDao = database.getRedditDb()

    override suspend fun insert(redditData: List<RedditTopModel>) {
        RedditMapper.mapListRedditModelToListRedditEntity(redditData)?.let { databaseDao.insert(it) }
    }

    override suspend fun delete(redditData: List<RedditTopModel>) {
        RedditMapper.mapListRedditModelToListRedditEntity(redditData)?.let { databaseDao.delete(it) }
    }

    override suspend fun getAllRedditData(): List<RedditTopModel>? =
        RedditMapper.mapListRedditEntityToListRedditModel(databaseDao.getAllRedditData())

    override suspend fun getTopRedditPosts(subreddit: String, pageSize: Int): ListingResponse? {
        var requestModel: ListingResponse? = null
        val job = CoroutineScope(Dispatchers.IO).launch {
            val v = api.getTop(subreddit, pageSize)
            withContext(Dispatchers.IO){
                try {
                    val a = v.await()
                    if (a.isSuccessful)
                        requestModel = RedditMapper.mapDataModelToDomainModel(a.body())
                    else
                        requestModel = Gson().fromJson(a.errorBody()?.string(), ListingResponse::class.java)
                }catch (e: HttpException){
                    Log.e("HttpException", e.message())
                }catch (e: Throwable){
                    Log.e("Throwable", e.message)
                }
            }
        }
        job.join()
//        val v = RedditMapper.mapDataModelToDomainModel(api.getTop(subreddit, pageSize).await())
        return requestModel
    }

    override suspend fun getTopRedditPostsAfter(subreddit: String, after: String, pageSize: Int): ListingResponse? =
        RedditMapper.mapDataModelToDomainModel(api.getTopAfter(subreddit, after, pageSize).await())

    override suspend fun getTopRedditPostsBefore(subreddit: String, before: String, pageSize: Int): ListingResponse? =
        RedditMapper.mapDataModelToDomainModel(api.getTopBefore(subreddit, before, pageSize).await())

    override suspend fun clearTable() = databaseDao.nukeTable()
}