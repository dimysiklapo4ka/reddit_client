package dimysiklapo4ka.com.data.repository.mappers

import android.util.Log
import com.google.gson.Gson
import dimysiklapo4ka.com.data.datasource.api.model.DataListingResponse
import dimysiklapo4ka.com.domain.models.ListingResponse
import dimysiklapo4ka.com.data.datasource.api.model.RedditChildrenResponse
import dimysiklapo4ka.com.data.datasource.database.model.RedditEntity
import dimysiklapo4ka.com.domain.models.RedditTopModel

object RedditMapper {

    fun mapRedditEntityToRedditModel(redditEntity: RedditEntity?): RedditTopModel? =
        if (redditEntity == null) null
        else Gson().fromJson(Gson().toJson(redditEntity), RedditTopModel::class.java)

    fun mapRedditModelToRedditEntity(redditModel: RedditTopModel?): RedditEntity? =
        if (redditModel == null) null
        else Gson().fromJson(Gson().toJson(redditModel), RedditEntity::class.java)

    fun mapListRedditEntityToListRedditModel(redditEntity: List<RedditEntity>?): List<RedditTopModel>?{
        val discountCardModels = arrayListOf<RedditTopModel>()
        redditEntity?.forEach {
            mapRedditEntityToRedditModel(it)?.let { it1 -> discountCardModels.add(it1) }
        }
        return discountCardModels
    }

    fun mapListRedditModelToListRedditEntity(redditModel: List<RedditTopModel>?): List<RedditEntity>?{
        val discountCardEntities = arrayListOf<RedditEntity>()
        redditModel?.forEach {
            mapRedditModelToRedditEntity(it)?.let { it1 -> discountCardEntities.add(it1) }
        }
        return discountCardEntities
    }

    fun mapListRedditChildrenToListRedditModel(redditEntity: List<RedditChildrenResponse>?): List<RedditTopModel>?{
        val discountCardModels = arrayListOf<RedditTopModel>()
        redditEntity?.forEach {
            mapRedditEntityToRedditModel(it.data)?.let { it1 -> discountCardModels.add(it1) }
        }
        return discountCardModels
    }

    fun mapDataModelToDomainModel(response: DataListingResponse?): ListingResponse?{
//        Log.e("232323232", response?.message)
        return Gson().fromJson(Gson().toJson(response), ListingResponse::class.java)
    }

}