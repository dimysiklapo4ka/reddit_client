package dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dimysiklapo4ka.com.domain.models.RedditChildrenResponse
import dimysiklapo4ka.com.yalantisredditclient.R
import dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.viewholder.RedditViewHolder

class RedditAdapter(diffCallback: DiffUtil.ItemCallback<RedditChildrenResponse>) : PagedListAdapter<RedditChildrenResponse, RedditViewHolder>(diffCallback) /*RecyclerView.Adapter<RedditViewHolder>()*/{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RedditViewHolder =
        RedditViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_reddit_item, parent, false))

    override fun onBindViewHolder(holder: RedditViewHolder, position: Int) {
        getItem(position)?.data?.let { holder.bindView(it) }
    }

//    val redditList = arrayListOf<RedditChildrenResponse>()
//
//    fun setItems(items: List<RedditChildrenResponse>){
//        redditList.addAll(items)
//        notifyDataSetChanged()
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RedditViewHolder =
//        RedditViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_reddit_item, parent, false))
//
//    override fun getItemCount(): Int = redditList.size
//
//    override fun onBindViewHolder(holder: RedditViewHolder, position: Int) {
//        holder.bindView(redditList[position].data)
//    }
}