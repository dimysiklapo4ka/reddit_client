package dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.viewholder

import android.os.Build
import android.text.Html
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dimysiklapo4ka.com.domain.models.RedditTopModel
import dimysiklapo4ka.com.yalantisredditclient.R
import dimysiklapo4ka.com.yalantisredditclient.extension.formatDate
import dimysiklapo4ka.com.yalantisredditclient.extension.visibilityView
import kotlinx.android.synthetic.main.view_reddit_item.view.*
import java.util.*
import androidx.browser.customtabs.CustomTabsIntent
import android.net.Uri

class RedditViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bindView(model: RedditTopModel) {
        model.subreddit?.let { itemView.tv_subreddit.text = it }
        model.author?.let { itemView.tv_author.text = it }
        model.created_utc?.let { itemView.tv_post_date.text = formatDate(Date(1000L * it)) }
        model.title?.let { itemView.tv_title.text = it }
        if (model.selftext.isNullOrBlank()) itemView.tv_description.visibilityView(false)
        else {
            itemView.tv_description.visibilityView(true)
            if (Build.VERSION.SDK_INT <= 24)
                itemView.tv_description.text = Html.fromHtml(model.selftext)
            else
                itemView.tv_description.text = Html.fromHtml(model.selftext, Html.FROM_HTML_MODE_COMPACT)
        }
        model.thumbnail?.let {
            Glide.with(itemView.context)
                .load(it)
                .placeholder(R.drawable.placeholder)
                .into(itemView.iv_groupe_image)
        }
        model.score?.let { itemView.tv_rating.text = "Score: $it" }
        model.num_comments?.let { itemView.tv_comments.text = "Comments: $it" }

        model.url?.let {
            itemView.setOnClickListener { _ ->
                val builder = CustomTabsIntent.Builder()
                val customTabsIntent = builder.build()
                customTabsIntent.launchUrl(itemView.context, Uri.parse(it))
            }
        }
    }

}