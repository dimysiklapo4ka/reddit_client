package dimysiklapo4ka.com.yalantisredditclient.ui.screens.main

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import dimysiklapo4ka.com.domain.models.RedditChildrenResponse
import dimysiklapo4ka.com.yalantisredditclient.R
import dimysiklapo4ka.com.yalantisredditclient.base.BaseFragment
import dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.adapter.RedditAdapter
import dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.adapter.RedditDiffUtils
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.android.ext.android.inject

class MainFragment: BaseFragment<MainViewModel>() {
    override val resLayout: Int = R.layout.fragment_main
    override val isNeedActionBarMenu: Boolean = true
    override val resMenu: Int = R.menu.search_menu
    override val viewModel: MainViewModel by inject()
    private val diffCallback = RedditDiffUtils()
    private val adapter = RedditAdapter(diffCallback)

    private val redditTopListObserver = Observer<PagedList<RedditChildrenResponse>> { adapter.submitList(it) }
    private val loadingObserver = Observer<Boolean>{ baseActivity().loadingProgressVisibility(it) }
    private val errorObserver = Observer<String> { baseActivity().globalError(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_reddit.adapter = adapter
        getDataInRedditRepository("")
    }

    override fun menuInflater(menu: Menu?, inflater: MenuInflater?) {

        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchItem = menu?.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView

        searchView.setIconifiedByDefault(false)
        searchView.imeOptions = EditorInfo.IME_ACTION_SEARCH
        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { its ->
                    getDataInRedditRepository(its)
                }
                searchItem.collapseActionView()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                return false
            }
        })
//        searchItem.expandActionView()
    }

    private fun getDataInRedditRepository(subbredit: String){
        viewModel.getRedditTopList(subbredit).observe(this@MainFragment, redditTopListObserver)
        viewModel.getLoadingLiveData()?.observe(this@MainFragment, loadingObserver)
        viewModel.getErrorLiveData()?.observe(this@MainFragment, errorObserver)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun observeLiveData(viewModel: MainViewModel) {
        with(viewModel){

        }
    }
}