package dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.adapter

import androidx.recyclerview.widget.DiffUtil
import dimysiklapo4ka.com.domain.models.RedditChildrenResponse

class RedditDiffUtils: DiffUtil.ItemCallback<RedditChildrenResponse>() {
    override fun areItemsTheSame(oldItem: RedditChildrenResponse, newItem: RedditChildrenResponse): Boolean =
        oldItem.data.author == newItem.data.author

    override fun areContentsTheSame(oldItem: RedditChildrenResponse, newItem: RedditChildrenResponse): Boolean =
            oldItem.data == newItem.data

}