package dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.datasources

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import dimysiklapo4ka.com.domain.interactors.RedditInteractor
import dimysiklapo4ka.com.domain.models.RedditChildrenResponse
import dimysiklapo4ka.com.domain.models.RedditTopModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RedditApiDataSource(
    private val redditInteractor: RedditInteractor,
    private val subreddit: String
) : PageKeyedDataSource<String, RedditChildrenResponse>() {

    private val loadingLiveData = MutableLiveData<Boolean>()
    private val errorLiveData = MutableLiveData<String>()

    fun getloadingLiveData(): MutableLiveData<Boolean> = loadingLiveData
    fun geterrorLiveData(): MutableLiveData<String> = errorLiveData

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<String, RedditChildrenResponse>
    ) {
        loadingLiveData.postValue(true)
        GlobalScope.launch {
            try {
                val apiJob = withContext(Dispatchers.IO) {
                    redditInteractor.getTopRedditPosts(subreddit, params.requestedLoadSize)
                }
                apiJob?.let {
                    it.data?.children?.toMutableList()?.let {
                            it1 -> callback.onResult(it1, it.data?.before, it.data?.after)
                    }
                    saveToDataBase(it.data?.children)
                    it.message?.let { its ->
                        if (!subreddit.isBlank())
                            errorLiveData.postValue(its)
                        callback.onResult(getDatabaseData(), null, null)
                    }
                }
                loadingLiveData.postValue(false)
            } catch (e: Exception) {
                if (!subreddit.isBlank())
                    errorLiveData.postValue(e.message)
                loadingLiveData.postValue(false)
            }

        }
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, RedditChildrenResponse>) {
        loadingLiveData.postValue(true)
        GlobalScope.launch {
            try {
                val job = withContext(Dispatchers.IO) {
                    redditInteractor.getTopRedditPostsAfter(subreddit, params.key, params.requestedLoadSize)
                }
                job?.let {
                    it.data?.children?.toMutableList()?.let { it1 -> callback.onResult(it1, it.data?.after) }
                }
                loadingLiveData.postValue(false)
            } catch (e: Exception) {
                errorLiveData.postValue(e.message)
                loadingLiveData.postValue(false)
            }
        }
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, RedditChildrenResponse>) {
        loadingLiveData.postValue(true)
        GlobalScope.launch {
            try {

                val job = withContext(Dispatchers.IO) {
                    redditInteractor.getTopRedditPostsBefore(subreddit, params.key, params.requestedLoadSize)
                }
                job?.let {
                    it.data?.children?.toMutableList()?.let { it1 -> callback.onResult(it1, it.data?.before) }
                }
                loadingLiveData.postValue(false)
            } catch (e: Exception) {
                errorLiveData.postValue(e.message)
                loadingLiveData.postValue(false)
            }
        }
    }

    private fun saveToDataBase(saves: List<RedditChildrenResponse>?) {
        saves?.let {
            val models = mutableListOf<RedditTopModel>()
            it.forEach { its ->
                models.add(its.data)
            }
            if (models.isNotEmpty()) {
                GlobalScope.launch {
                    redditInteractor.clearTable()
                    redditInteractor.insert(models)
                }
            }
        }
    }

    private suspend fun getDatabaseData(): List<RedditChildrenResponse> {
        val pagingDbList = mutableListOf<RedditChildrenResponse>()
        val job = GlobalScope.launch {
            val allModels = withContext(Dispatchers.IO) {
                redditInteractor.getAllRedditData()
            }
            allModels?.forEach {
                pagingDbList.add(RedditChildrenResponse(it))
            }
        }
        job.join()
        return pagingDbList
    }
}