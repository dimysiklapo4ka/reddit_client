package dimysiklapo4ka.com.yalantisredditclient.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import dimysiklapo4ka.com.domain.interactors.RedditInteractor
import dimysiklapo4ka.com.yalantisredditclient.R
import dimysiklapo4ka.com.yalantisredditclient.base.BaseActivity
import dimysiklapo4ka.com.yalantisredditclient.extension.fragmentReplace
import dimysiklapo4ka.com.yalantisredditclient.extension.visibilityView
import dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.MainFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadingProgressVisibility(false)
        fragmentReplace(MainFragment(), null, false)
    }

    override fun loadingProgressVisibility(visibility: Boolean) {
        pb_loading.visibilityView(visibility)
    }

}
