package dimysiklapo4ka.com.yalantisredditclient.ui.screens.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import dimysiklapo4ka.com.domain.interactors.RedditInteractor
import dimysiklapo4ka.com.domain.models.RedditChildrenResponse
import dimysiklapo4ka.com.yalantisredditclient.base.BaseViewModel
import dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.datasources.RedditApiDataSource
import dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.datasources.RedditApiDataSourceFactory

class MainViewModel(
    private val redditInteractor: RedditInteractor
): BaseViewModel() {

    private lateinit var pagedLiveData: LiveData<PagedList<RedditChildrenResponse>>
    private var loadingDSLiveData:  LiveData<Boolean>? = MutableLiveData<Boolean>()
    private var errorDSLiveData: LiveData<String>? = null

    fun getRedditTopList(subreddit:String):LiveData<PagedList<RedditChildrenResponse>>{

        val dataSourceFactory = RedditApiDataSourceFactory(redditInteractor, subreddit)

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(10)
            .setPageSize(10).build()

        pagedLiveData = LivePagedListBuilder(dataSourceFactory, pagedListConfig).build()

        loadingDSLiveData = Transformations.switchMap(dataSourceFactory.getDataSource(), RedditApiDataSource::getloadingLiveData)
        errorDSLiveData = Transformations.switchMap(dataSourceFactory.getDataSource(), RedditApiDataSource::geterrorLiveData)

        return pagedLiveData
    }

    fun getLoadingLiveData(): LiveData<Boolean>? = loadingDSLiveData
    fun getErrorLiveData(): LiveData<String>? = errorDSLiveData

}