package dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.datasources

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import dimysiklapo4ka.com.domain.interactors.RedditInteractor
import dimysiklapo4ka.com.domain.models.RedditChildrenResponse

class RedditApiDataSourceFactory(private val redditInteractor: RedditInteractor,
                                 private val subreddit: String)
    : DataSource.Factory<String, RedditChildrenResponse>() {

    private val dataSourceLiveData = MutableLiveData<RedditApiDataSource>()

    fun getDataSource(): MutableLiveData<RedditApiDataSource> = dataSourceLiveData

    override fun create(): DataSource<String, RedditChildrenResponse> {
        val dataSource = RedditApiDataSource(redditInteractor, subreddit)
        dataSourceLiveData.postValue(dataSource)
        return dataSource
    }

}