package dimysiklapo4ka.com.yalantisredditclient.extension

import android.os.Bundle
import android.transition.Slide
import android.view.Gravity
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import dimysiklapo4ka.com.yalantisredditclient.R
import java.text.SimpleDateFormat
import java.util.*

fun View.visibilityView(visibility: Boolean) = if (visibility) this.visibility = View.VISIBLE
else this.visibility = View.GONE

fun FragmentActivity.fragmentReplace(fragment: Fragment, argument: Bundle?, needToBackStack: Boolean = true) {
    supportFragmentManager.beginTransaction().apply {
        argument?.let { fragment.arguments = it }
//        fragment.enterTransition = Slide(Gravity.END) TODO add enter transaction
//        fragment.exitTransition = Slide(Gravity.START) TODO add exit transaction
        this.replace(R.id.container, fragment, fragment::class.java.simpleName)
        if (needToBackStack) {
            addToBackStack(fragment::class.java.simpleName)
        }
    }.commit()
}

fun FragmentActivity.popBackStack() {
    supportFragmentManager.popBackStack()
}

fun RecyclerView.ViewHolder.formatDate(date: Date): String{
    val time = System.currentTimeMillis() - date.time
    val day = time / (1000 * 60 * 60 * 24)
    val hour = time / (1000 * 60 * 60)
    val minute = time / (1000 * 60) % 60
    var string = ""

    string = if (day == 0L) {

        if (hour > 0) {
            "$hour hour ago"
        } else {
            if (minute > 0) {
                "$minute minutes ago"
            } else {
                "Just now"
            }
        }
    } else {
        SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault()).format(date)
    }
    return string
}