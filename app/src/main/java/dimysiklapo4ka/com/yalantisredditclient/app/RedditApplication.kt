package dimysiklapo4ka.com.yalantisredditclient.app

import android.app.Application
import dimysiklapo4ka.com.data.dataInjector
import dimysiklapo4ka.com.domain.domainInjector
import dimysiklapo4ka.com.yalantisredditclient.appInjector
import org.koin.android.ext.android.startKoin

class RedditApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        val dependencies = listOf(appInjector, dataInjector, domainInjector)
        startKoin(this, dependencies)
    }
}