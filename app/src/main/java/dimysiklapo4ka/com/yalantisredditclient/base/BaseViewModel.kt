package dimysiklapo4ka.com.yalantisredditclient.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

open class BaseViewModel: ViewModel() {

    var errorLiveData = MutableLiveData<String>()
    var loadingLiveData = MutableLiveData<Boolean>()

    private var viewModelJob = Job()
    private val viewModelScoupe = CoroutineScope(/*Dispatchers.Main + */viewModelJob)

    fun <P> workInIOThread(doOnAsyncBlock: suspend CoroutineScope.() -> P){
        doCoroutineWork(doOnAsyncBlock, viewModelScoupe, Dispatchers.IO)
    }

    fun <P> workInMainThread(doOnAsyncBlock: suspend CoroutineScope.() -> P){
        doCoroutineWork(doOnAsyncBlock, viewModelScoupe, Dispatchers.Main)
    }

    private inline fun <P> doCoroutineWork(
        crossinline doOnAsyncBlock: suspend CoroutineScope.() -> P,
        coroutineScope: CoroutineScope,
        context: CoroutineContext) {

        coroutineScope.launch {
            withContext(context){
                doOnAsyncBlock.invoke(this)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancelChildren()
    }
}