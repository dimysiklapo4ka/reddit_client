package dimysiklapo4ka.com.yalantisredditclient.base

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity: AppCompatActivity() {

    abstract fun loadingProgressVisibility(visibility: Boolean)
    fun globalError(errorText: String){
        Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show()
    }

}