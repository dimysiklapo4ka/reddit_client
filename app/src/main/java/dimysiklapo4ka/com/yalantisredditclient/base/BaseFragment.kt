package dimysiklapo4ka.com.yalantisredditclient.base

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import dimysiklapo4ka.com.yalantisredditclient.R

abstract class BaseFragment<VM: BaseViewModel>: Fragment(){
    protected abstract val resLayout: Int
    protected abstract val isNeedActionBarMenu: Boolean
    protected abstract val resMenu: Int
    protected abstract val viewModel: VM

    private val loadingObserver: Observer<Boolean> = Observer {
        baseActivity().loadingProgressVisibility(it)
    }
    private val errorObserver: Observer<String> = Observer {
        baseActivity().globalError(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeAllLiveData(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(resLayout, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(isNeedActionBarMenu)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(resMenu, menu)
        menuInflater(menu, inflater)
        super.onCreateOptionsMenu(menu, inflater)
    }

    protected abstract fun observeLiveData(viewModel: VM)

    protected abstract fun menuInflater(menu: Menu?, inflater: MenuInflater?)

    private fun observeAllLiveData(viewModel: VM){
        observeLiveData(viewModel)
        with(viewModel){
            errorLiveData.observe(this@BaseFragment, errorObserver)
            loadingLiveData.observe(this@BaseFragment, loadingObserver)
        }
    }

    fun baseActivity(): BaseActivity {
        return (activity as BaseActivity)
    }
}