package dimysiklapo4ka.com.yalantisredditclient

import dimysiklapo4ka.com.yalantisredditclient.ui.screens.main.MainViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appInjector = module {

        viewModel { MainViewModel(get()) }
}